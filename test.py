import matplotlib.pyplot as plt
import numpy as np

def autocor(X, lags=300):
    COR = np.zeros((lags,2))
    for i in range(X.shape[0]):
        for t in range(lags):
            if i+t >= X.shape[0]:
                break
            COR[t,0] += X[i] * X[i+t] 
            COR[t,1] += 1
        
    return COR[:,0] / (1+COR[:,1])

fig, ax = plt.subplots()
ax.set_title("Ar HFAF")

cors = []

for r in [1,2,4]:
    HF = np.loadtxt(f"{r}/HEATFLUX")[:,3:6]
    
    y = autocor(HF[:,0])
    x = np.linspace(0, y.shape[0], y.shape[0])
    cors.append((x,y))
    ax.scatter(x, y, label=f"DL_POLY np {r}")
    
corLmp = np.loadtxt("lmp/lmp-heatflux") / 0.001

y = autocor(corLmp[:, 0])
x = np.linspace(0, y.shape[0], y.shape[0])
ax.scatter(x, y, label="LAMMPS")

ax.legend()

fig.savefig("Ar-hfaf.png")

error = np.mean(np.sqrt((cors[0][1]-cors[1][1])**2))
print(f'DL_POLY Self consistency: Error {error}')
error_lmp = np.mean(np.sqrt((cors[0][1]-y)**2))
print(f'DL_POLY-LMP: Error {error_lmp}')
assert error < 1e-9
assert error_lmp < 1e-9
