#!/usr/bin/env python3
"""
Script to convert from lammps dumps to DLPOLY input
Originally Written by A. M. Elena
Modified by J. Wilkins
"""

class AtomicSystem():
    """ A type to represent a dlp system """
    def __init__(self, filename, fileType=None):
        self.filename = filename
        self.keys = []
        self.imcon = 0
        self.cell = None
        self.atoms = None
        self.nAtoms = 0
        self.name = self.filename.split(".")[0]
        if not fileType:
            if self.filename.endswith(".xyz"):
                self.fileType = "xyz"
                self.cell, self.atoms = self.read_xyz()
            elif self.filename.endswith(".atom"):
                self.fileType = "atom"
                self.cell, self.atoms = self.read_dump_custom()
            elif self.filename.endswith("CONFIG") or self.filename.endswith(".config"):
                self.fileType = "dlp"
                self.cell, self.atoms = self.read_config()
            else:
                self.fileType = "custom"
                self.cell, self.atoms = self.read_dump_custom()
        else:
            self.fileType = fileType

    hasPos = property(lambda self: self.hasScPos or self.hasRawPos)
    hasScPos = property(lambda self: ("xs" in self.keys and "ys" in self.keys and "zs" in self.keys))
    hasRawPos = property(lambda self: ("x" in self.keys and "y" in self.keys and "z" in self.keys))
    hasVel = property(lambda self: ("vx" in self.keys and "vy" in self.keys and "vz" in self.keys))
    hasFor = property(lambda self: ("fx" in self.keys and "fy" in self.keys and "fz" in self.keys))
    
    @property
    def configKey(self):
        """ Return the valid config key """
        configKey = 0
        if self.hasVel:
            configKey += 1
            if self.hasFor:
                configKey += 1
        return configKey

    def _read_atoms(self, keys, fileIn, toRead=None):
        """ Read atom data based on keys """
        atom = []
        for atomCount, line in enumerate(fileIn):
            atom.append({keys[i]:float(val) for i, val in enumerate(line.split())})
            if "id" not in keys:
                atom[atomCount]["id"] = atomCount + 1
            if toRead and atomCount+1 == toRead:
                break
        if "id" not in keys:
            self.keys.append("id")
            
        if self.nAtoms != atomCount+1:
            raise IndexError("Bad number of atoms found in file. Expected {}, found {}."
                             .format(self.nAtoms, atomCount+1))
        return atom

    def read_dump_custom(self):
        """ Read custom dump file by file header """
        with open(self.filename, "r") as openFile:
            while True:
                line = openFile.readline()
                if not line:
                    break
                if line.startswith("ITEM:"):
                    _, key = line.split(": ")

                    if "TIMESTEP" in key:
                        openFile.readline() # Skip
                    elif "NUMBER OF ATOMS" in key:
                        self.nAtoms = int(openFile.readline())
                    elif "BOX BOUNDS" in key:
                        if len(key.split()) == 5: # Orthogonal
                            self.imcon = 1
                            outCell = []
                            for i in range(3):
                                cellLine = openFile.readline().strip().split()
                                tmp = [0.0]*3
                                tmp[i] = float(cellLine[1])
                                outCell.append(tmp)
                        else: # Non-orthog
                            # Calc cell vecs here
                            self.imcon = 3
                            raise NotImplementedError("Unable to do non-orthog boundaries")
                    elif "ATOMS" in key:
                        *self.keys, = key.split()[1:]
                        atoms = self._read_atoms(self.keys, openFile)
        return outCell, atoms

    def read_xyz(self):
        """ Read a standard xyz file """
        with open(self.filename, "r") as openFile:
            self.nAtoms = int(openFile.readline().split()[0])
            openFile.readline() # Comment
            self.keys = ["ID", "x", "y", "z"]
            atoms = self._read_atoms(self.keys, openFile)
        return None, atoms

    def read_config(self):
        """ Read a DLPOLY Config file """
        with open(self.filename) as openFile:
            self.name = openFile.readline() # Title
            configKey, self.imcon, *self.nAtoms = map(int, openFile.readline.split())
            self.nAtoms = int(*self.nAtoms) or None
            keys = ("x", "y", "z"), ("vx", "vy", "vz"), ("fx", "fy", "fz")
            self.keys = ["spec", "ID"] + [x for key in keys[:configKey] for x in key]
            outCell = [openFile.readline().strip().split() for _ in range(3)]

            atomCount = 0
            atoms = []
            while openFile:
                vals = openFile.readline().strip().split()
                atoms.append({key:val for key, val in zip(("spec", "id"), vals)})
                for i in range(configKey+1):
                    line = openFile.readline()
                    for keyInd, val in enumerate(line.split()):
                        key = self.keys[2 + 3*i + keyInd]
                        atoms[atomCount][key] = val
                atomCount += 1
        if self.nAtoms and atomCount != self.nAtoms:
            raise IndexError("Bad number of atoms found in file. Expected {}, found {}."
                             .format(self.nAtoms, atomCount))

        return outCell, atoms

    def write_dlp(self, filename):
        """ Write DLPOLY Config file """
        if not self.hasPos or not self.atoms or not self.cell:
            raise TypeError("Insufficient information to generate DLPOLY CONFIG")

        floatSpec = "{: >20.10f}"
        intSpec = "{: >10d}"
        charSpec = "{: <8s}"
        
        with open(filename, "w") as openFile:
            print(self.name, file=openFile)
            print((intSpec*3).format(self.configKey, self.imcon, len(self.atoms)), file=openFile)
            for vec in self.cell:
                print((floatSpec*3).format(*map(float, vec)), file=openFile)
            for atomCount, atom in enumerate(self.atoms):
                print(charSpec.format(atom["spec"]) if "spec" in self.keys else intSpec.format(atom["id"]),
                      intSpec.format(atomCount+1), file=openFile)
                if self.hasRawPos:
                    print(*(floatSpec.format(atom[direc]) for direc in "xyz"), file=openFile)
                else:
                    print(*(
                        floatSpec.format(atom[direc+"s"] * self.cell[dim][dim])
                        for dim, direc in enumerate("xyz")), file=openFile)
                if self.configKey > 0:
                    print(*(floatSpec.format(atom["v"+direc]) for direc in "xyz"), file=openFile)
                if self.configKey > 1:
                    print(*(floatSpec.format(atom["f"+direc]) for direc in "xyz"), file=openFile)
                
config = AtomicSystem("dump.atom")
for atom in config.atoms:
    atom["spec"] = "Ar"
    config.keys.append("spec")
    # ps vs fs
    atom["vx"] *= 1000.
    atom["vy"] *= 1000.
    atom["vz"] *= 1000.

config.write_dlp("TEST.config")
