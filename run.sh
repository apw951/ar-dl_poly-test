for r in 1 2 4
do
	rm -rf $r
	mkdir $r
	cp Ar.* $r/
	cd $r
	mpirun -np $r --use-hwthread-cpus --oversubscribe ~/DLPOLY.Z -c Ar.control
	cd ..
done
