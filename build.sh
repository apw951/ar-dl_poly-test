cd dl-poly
rm -rf build
mkdir build
cd build
cmake .. -DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=Release -DWITH_ASSERT=OFF -DDEBUG=OFF -DWITH_EXP=OFF && make
cp bin/DLPOLY.Z ~/
cd ~/
