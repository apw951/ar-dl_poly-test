import numpy as np
from tqdm import tqdm
import argparse 
parser = argparse.ArgumentParser()
parser.add_argument("t", type=int, help="timesteps")

args = parser.parse_args()
t = args.t
f = open('log.lammps', 'r')

lines = f.readlines()

start = ['Step v_Jx v_Jy v_Jz' in l for l in lines].index(True)+1

data = lines[start:start+t]

HF = np.zeros((t, 3))
for i in tqdm(range(t)):
    line = data[i]
    HF[i,:] = line.split()[1:4]

np.savetxt("lmp-heatflux", HF)
