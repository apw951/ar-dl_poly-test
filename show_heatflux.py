import numpy as np
import matplotlib.pyplot as plt

dl = np.loadtxt("1/HEATFLUX")[0:-1, 3:6]
lmp = np.loadtxt("lmp/lmp-heatflux") / 0.001

fig, ax = plt.subplots(1)

ts = [t for t in range(dl.shape[0])]

ax.scatter(ts, dl[:, 0], label="DL_POLY")
ax.scatter(ts, lmp[:, 0], label="LAMMPS")
ax.legend()
fig.savefig("heatflux.png")

